#include "threads.h"

int main()
{
	// section 1
	call_I_Love_Threads();
	// section 2
	vector<int> primes;
	getPrimes(58, 89, primes);
	printVector(primes);
	primes = callGetPrimes(93, 289);
	printVector(primes);
	// section 3
	primes = callGetPrimes(0, 1000);
	primes = callGetPrimes(0, 10000);
	primes = callGetPrimes(0, 1000000);
	// section 4 + 5
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, 100000, "primes3.txt", 5);
	callWritePrimesMultipleThreads(0, 1000000, "primes4.txt", 10);

	system("pause");
	return 0;
}