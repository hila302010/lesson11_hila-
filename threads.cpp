#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	// at this point thread t1 is running
	t1.join(); // wait until t1 will end
}

void printVector(vector<int> primes)
{
	cout << "vector of primes: " << endl;
	for (int i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	bool isPrime = true; // prime
	for (int i = begin; i <= end; i++)
	{
		// check if prime
		isPrime = true;
		for (int j = 2; j <= sqrt(i); j++)
			if (i % j == 0) // if it is devieded - it is not a prime
				isPrime = false; // not prime
		// if prime:
		if (isPrime)
			primes.push_back(i);
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> myVector; 
	clock_t beginTime = clock();
	// create a thread
	thread _thread(getPrimes, begin, end, ref(myVector));
	_thread.join(); // wait until thread will end
	clock_t endTime = clock();
	cout << "The time that took the thread to get primes between  " << begin << " to  " << end << " is: " << endTime - beginTime << " ms" << endl;
	return myVector;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	bool isPrime = true; // prime
	for (int i = begin; i <= end; i++)
	{
		// check if prime
		isPrime = true;
		for (int j = 2; j <= sqrt(i); j++)
			if (i % j == 0) // if it is devieded - it is not a prime
				isPrime = false; // not prime
		// if prime:
		if (isPrime)
			file << "\t" << i << "\t"; // add to file
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	// open file
	ofstream myFile;
	myFile.open(filePath);
	// create vector of threads
	vector<thread> threads;
	int lenOfThread = (end - begin) / N;
	int currBegin = begin;
	clock_t beginTime = clock();
	for (int i = 0; i < N; i++)
	{
		// add thread to the vector
		threads.push_back(thread(writePrimesToFile, currBegin, currBegin + lenOfThread, ref(myFile)));
		currBegin += lenOfThread;
	}
	// wait until the all the threads end
	for (int j = 0; j < N; j++)
	{
		threads[j].join();
	}
	clock_t endTime = clock();
	cout << "The time that took the threads to write primes between  " << begin << " to  " << end << " is: " << endTime - beginTime << " ms"<< endl;
}
